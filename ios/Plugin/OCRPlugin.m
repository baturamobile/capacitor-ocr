#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

// Define the plugin using the CAP_PLUGIN Macro, and
// each method the plugin supports using the CAP_PLUGIN_METHOD macro.
CAP_PLUGIN(OCRPlugin, "OCR",
           CAP_PLUGIN_METHOD(process, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(processText, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(processBarcode, CAPPluginReturnPromise);
)
