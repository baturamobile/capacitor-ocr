import Foundation
import Capacitor

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(OCRPlugin)
public class OCRPlugin: CAPPlugin {
    private let implementation = OCR()

    @objc func process(_ call: CAPPluginCall) {
        processText(call)
    }

    @objc func processText(_ call: CAPPluginCall) {
        guard let value = call.getString("image") else {
            call.reject("IMAGE_PARAMETER_REQUIRED")
            return
        }
        textRecognizer(call, image: value)
    }

    @objc func processBarcode(_ call: CAPPluginCall) {
        guard let value = call.getString("image") else {
            call.reject("IMAGE_PARAMETER_REQUIRED")
            return
        }
        barcodeRecognizer(call, image: value)
    }

    private func textRecognizer(_ call: CAPPluginCall, image: String) {
        implementation.onDeviceTextRecognizer(image: image) { text, error in
            if let error = error {
                call.reject(error.localizedDescription)
            } else {
                call.resolve([
                    "text": text!
                ])
            }
        }
    }

    private func barcodeRecognizer(_ call: CAPPluginCall, image: String) {
        implementation.onBarcodeRecognizer(image: image) { values, error in
            if let error = error {
                call.reject(error.localizedDescription)
            } else {
                call.resolve([
                    "values": values!
                ])
            }
        }
    }
}
