import Foundation
import MLKitTextRecognition
import MLKitVision
import UIKit
import MLKitBarcodeScanning

@objc public class OCR: NSObject {    
    @objc public func onDeviceTextRecognizer(image: String, _ completion: @escaping (String?, Error?) -> Void) {
        getImage(imageURL: image) { (image, error) in
            if let error = error {
                completion(nil, error)
            } else {
                let options = TextRecognizerOptions()
                let textRecognizer = TextRecognizer.textRecognizer(options: options)
                let visionImage = VisionImage(image: image!)
                textRecognizer.process(visionImage) { (text, error) in
                    if let error = error {
                        completion(nil, error)
                    } else {
                        completion(text?.text ?? "", nil)
                    }
                }
            }
        }
    }

    @objc public func onBarcodeRecognizer(image: String, _ completion: @escaping ([String]?, Error?) -> Void) {
        getImage(imageURL: image) { (image, error) in
            if let error = error {
                completion(nil, error)
            } else {
                let barcodeRecognizer = BarcodeScanner.barcodeScanner()
                let visionImage = VisionImage(image: image!)
                barcodeRecognizer.process(visionImage) { (barcodes, error) in
                    if let error = error {
                        completion(nil, error)
                    } else {
                        let values = barcodes?.compactMap({ $0.rawValue }) ?? []
                        completion(values, nil)
                    }
                }
            }
        }
    }
    
    private func getImage(imageURL: String, _ completion: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
        let imageFormated = imageURL
            .replacingOccurrences(of: "data:image/jpeg;base64,", with: "")
            .replacingOccurrences(of: "data:image/png;base64,", with: "")
        if let data = Data(base64Encoded: imageFormated), let image = UIImage(data: data) {
            completion(image, nil)
        } else {
            guard let url = URL(string: imageURL) else {
                let error = NSError(domain: "capacitor-ocr",
                                    code: -1,
                                    userInfo: [NSLocalizedDescriptionKey : "URL_IMAGE_ERROR"])
                completion(nil, error)
                return
            }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(nil, error)
                } else {
                    guard let data = data, let image = UIImage(data: data) else {
                        let error = NSError(domain: "capacitor-ocr",
                                            code: -1,
                                            userInfo: [NSLocalizedDescriptionKey : "DOWNLOAD_IMAGE_ERROR"])
                        completion(nil, error)
                        return
                    }
                    completion(image, nil)
                }
            }
            .resume()
        }
    }
}


