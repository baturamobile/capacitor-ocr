import { registerPlugin } from '@capacitor/core';

import type { OCRPlugin } from './definitions';

const OCR = registerPlugin<OCRPlugin>('OCR', {
  web: () => import('./web').then(m => new m.OCRWeb()),
});

export * from './definitions';
export { OCR };
