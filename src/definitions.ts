export interface OCROptions {
  /**
   * It can be base64 and Uri format
   */
  image: string
}

export interface OCRResult {
  /**
   * Text of the processed image
   */
  text: string
}

export interface BarcodeResult {
  /**
   * List of all detected barcodes values
   */
  values: [string]
}

export interface OCRPlugin {
  /**
   * @deprecated Use processText(options:)
   * @param options
   */
  process(options: OCROptions): Promise<OCRResult>;
  processText(options: OCROptions): Promise<OCRResult>;
  processBarcode(options: OCROptions): Promise<BarcodeResult>;
}
