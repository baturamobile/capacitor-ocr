import { WebPlugin } from '@capacitor/core';

import type { BarcodeResult, OCROptions, OCRPlugin, OCRResult } from './definitions';

export class OCRWeb extends WebPlugin implements OCRPlugin {
  async process(_options: OCROptions): Promise<OCRResult> {
    throw new Error('Method not implemented.');
  }
  async processText(_options: OCROptions): Promise<OCRResult> {
    throw new Error('Method not implemented.');
  }
  async processBarcode(_options: OCROptions): Promise<BarcodeResult> {
    throw new Error('Method not implemented.');
  }
}
