package com.baturamobile.capacitor.ocr;

import androidx.annotation.CallSuper;

public class Callback<Object> {

    public void onResponse(Object dataResponse) {
        //nothing
    }

    @CallSuper
    public void onError(String stringError) {
        //nothing
    }
}
