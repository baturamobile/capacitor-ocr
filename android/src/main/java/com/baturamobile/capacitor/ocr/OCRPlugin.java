package com.baturamobile.capacitor.ocr;

import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

import org.json.JSONArray;

@CapacitorPlugin(name = "OCR")
public class OCRPlugin extends Plugin {

    private OCR implementation = new OCR();

    @PluginMethod
    public void process(PluginCall call) {
        String value = call.getString("image");
        textRecognizer(call, value);
    }

    @PluginMethod
    public void processText(PluginCall call) {
        String value = call.getString("image");
        textRecognizer(call, value);
    }

    @PluginMethod
    public void processBarcode(PluginCall call) {
        String value = call.getString("image");
        barcodeRecognizer(call, value);
    }

    private void barcodeRecognizer(PluginCall call, String image) {
        JSObject ret = new JSObject();
        implementation.onBarcodeRecognizer(image, bridge.getContext(), new Callback<JSONArray>() {
            @Override
            public void onResponse(JSONArray dataResponse) {
                super.onResponse(dataResponse);
                ret.put("values", dataResponse);
                call.resolve(ret);
            }

            @Override
            public void onError(String stringError) {
                super.onError(stringError);
                call.reject(stringError);
            }
        });
    }

    private void textRecognizer(PluginCall call, String image) {
        JSObject ret = new JSObject();
        implementation.onDeviceTextRecognizer(image, bridge.getContext(), new Callback<String>() {
            @Override
            public void onResponse(String dataResponse) {
                super.onResponse(dataResponse);
                ret.put("text", dataResponse);
                call.resolve(ret);
            }

            @Override
            public void onError(String stringError) {
                super.onError(stringError);
                call.reject(stringError);
            }
        });
    }
}