package com.baturamobile.capacitor.ocr;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;

import androidx.annotation.RequiresApi;

import com.google.mlkit.vision.barcode.BarcodeScanner;
import com.google.mlkit.vision.barcode.BarcodeScannerOptions;
import com.google.mlkit.vision.barcode.BarcodeScanning;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OCR {
    public void onDeviceTextRecognizer(String message, Context context, Callback<String> callback) {
        if (message != null && message.length() > 0) {
            try {
                InputImage image = getImage(message, context);
                TextRecognizer recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
                recognizer.process(image)
                        .addOnSuccessListener( text -> {
                            callback.onResponse(text.getText());
                        })
                        .addOnFailureListener( e -> {
                            callback.onError(e.getLocalizedMessage());
                        });
            } catch (Exception e) {
                callback.onError(e.getLocalizedMessage());
            }
        } else {
            Exception exception = new Exception("IMAGE_PARAMETER_REQUIRED");
            callback.onError(exception.getLocalizedMessage());
        }
    }

    public void onBarcodeRecognizer(String message, Context context, Callback<JSONArray> callback) {
        if (message != null && message.length() > 0) {
            try {
                InputImage image = getImage(message, context);
                BarcodeScannerOptions options = new BarcodeScannerOptions.Builder()
                        .setBarcodeFormats(Barcode.FORMAT_ALL_FORMATS)
                        .build();
                BarcodeScanner recognizer = BarcodeScanning.getClient(options);
                recognizer.process(image)
                        .addOnSuccessListener(barcodes -> {
                            JSONArray values = new JSONArray();
                            for (Barcode barcode : barcodes) {
                                values.put(barcode.getRawValue());
                            }
                            callback.onResponse(values);
                        })
                        .addOnFailureListener(e -> {
                            callback.onError(e.getLocalizedMessage());
                        });

            } catch (Exception e) {
                callback.onError(e.getLocalizedMessage());
            }
        } else {
            Exception exception = new Exception("IMAGE_PARAMETER_REQUIRED");
            callback.onError(exception.getLocalizedMessage());
        }
    }

    private InputImage getImage(String image, Context context) throws IOException {
        String imageFormatted = image
                .replace("data:image/png;base64,", "")
                .replace("data:image/jpeg;base64,", "");

        try {
            byte[] decodedString = Base64.decode(imageFormatted, Base64.DEFAULT);
            Bitmap bitMap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            return InputImage.fromBitmap(bitMap, 0);
        } catch (Exception e) {
            Uri uri = Uri.parse(image);
            return InputImage.fromFilePath(context, uri);
        }
    }
}
