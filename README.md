# @baturamobile/capacitor-ocr

OCR Capacitor plugin

## Install

```bash
npm install @baturamobile/capacitor-ocr
npx cap sync
```

## Versions

| Tag   | Capacitor |
| ----- | --------- |
| 0.0.4 | v4.0      |
| 0.1.0 | v5.0      |
| 0.2.0 | v6.0      |

## API

<docgen-index>

* [`process(...)`](#process)
* [`processText(...)`](#processtext)
* [`processBarcode(...)`](#processbarcode)
* [Interfaces](#interfaces)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### process(...)

```typescript
process(options: OCROptions) => Promise<OCRResult>
```

| Param         | Type                                              |
| ------------- | ------------------------------------------------- |
| **`options`** | <code><a href="#ocroptions">OCROptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#ocrresult">OCRResult</a>&gt;</code>

--------------------


### processText(...)

```typescript
processText(options: OCROptions) => Promise<OCRResult>
```

| Param         | Type                                              |
| ------------- | ------------------------------------------------- |
| **`options`** | <code><a href="#ocroptions">OCROptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#ocrresult">OCRResult</a>&gt;</code>

--------------------


### processBarcode(...)

```typescript
processBarcode(options: OCROptions) => Promise<BarcodeResult>
```

| Param         | Type                                              |
| ------------- | ------------------------------------------------- |
| **`options`** | <code><a href="#ocroptions">OCROptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#barcoderesult">BarcodeResult</a>&gt;</code>

--------------------


### Interfaces


#### OCRResult

| Prop       | Type                | Description                 |
| ---------- | ------------------- | --------------------------- |
| **`text`** | <code>string</code> | Text of the processed image |


#### OCROptions

| Prop        | Type                | Description                     |
| ----------- | ------------------- | ------------------------------- |
| **`image`** | <code>string</code> | It can be base64 and Uri format |


#### BarcodeResult

| Prop         | Type                  | Description                          |
| ------------ | --------------------- | ------------------------------------ |
| **`values`** | <code>[string]</code> | List of all detected barcodes values |

</docgen-api>
