import { Component, ElementRef, ViewChild } from '@angular/core';
import { Camera, CameraResultType } from '@capacitor/camera';
import { OCR } from '@batura/capacitor-ocr';
import { IonContent, IonHeader, IonModal, IonToolbar, isPlatform } from '@ionic/angular';
import { CameraPreview, CameraPreviewOptions, CameraPreviewPictureOptions, CameraSampleOptions } from '@capacitor-community/camera-preview';
import { OverlayEventDetail } from '@ionic/core/components';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild(IonModal) modal: IonModal;
  @ViewChild('modalContent', {read: ElementRef}) modalContent: ElementRef;
  @ViewChild('modalToolbar', {read: ElementRef}) modalToolbar: ElementRef;
  text: string

  constructor() { }

  async openCamera() {
    return Camera.getPhoto({ allowEditing: true, resultType: CameraResultType.Uri })
  }

  async processText() {
    const result = await this.openCamera()
    const value = await OCR.processText({ image: result.path })
    this.text = value.text
  }

  async processQr() {
    const result = await this.openCamera()
    const value = await OCR.processBarcode({ image: result.path })
    this.text = value.values.join(",")
  }

  cancel() {
    this.modal.dismiss(null, 'cancel');
  }

  async onDidPresent(event: Event) {
    const width = this.modalContent.nativeElement.clientWidth
    const height = this.modalContent.nativeElement.clientHeight
    const y = isPlatform('ios') ? Math.ceil(height / 2) : Math.ceil(height / 4)  
    const cameraPreviewOptions: CameraPreviewOptions = {
      x: 0,
      y: y,
      width: width,
      height: width,
      position: 'rear'
    };
    await CameraPreview.start(cameraPreviewOptions);
    this.takeScreenshot()
  }

  onWillDismiss(event: Event) {
    const ev = event as CustomEvent<OverlayEventDetail<string>>;
    CameraPreview.stop()
    if (ev.detail.role === 'confirm') {
      this.text = ev.detail.data
    }
  }

  async takeScreenshot() {
    let options: CameraSampleOptions = {}
    const result = await CameraPreview.captureSample(options)
    this.getQR(result.value)
  }

  async getQR(image: string) {
    const value = await OCR.processBarcode({ image: image })
    if (value.values.length > 0) {
      this.modal.dismiss(value.values.join(","), 'confirm');
    } else {
      this.takeScreenshot()
    }
  }
}
